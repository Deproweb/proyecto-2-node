const express = require('express');//Para hacer el servidor y las rutas
const passport = require('passport');//Para la autenticación
const session = require('express-session');//Para las sesiones
const MongoStore = require('connect-mongo');//cge las sesiones y las guarda
const path=require('path');//El path para los directorios públicos
//Requiere las rutas
const indexRouter = require('./routes/index.routes');
const moviesRouter = require('./routes/movie.routes');
const authRouter = require('./routes/auth.routes');

//En esta carpeta, tenemos el serialize, el deserialize, y los passport use con las estrategias
require('./auth') //Por defecto, cuando hacemos require de una sola carpeta, es como hacer require del index

//Inicia la base de datos
const {connectToDb, DB_URL} = require('./config/db');
connectToDb();
const PORT = 3000;
const server = express();// Se arranca el servidor

server.use(express.static(path.join(__dirname + '/public')));//Unirlo, el path junta el directorio actual con el público.

//Sesión y cookie
server.use(session({
    secret: 'ASFDEWerefef654654&sd123',
    resave: false, //Solo guardará la sesión si hay cambios en ella.
    saveUnitialized: false, //Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
    cookie: {
        maxAge: 24 * 60 * 60 * 1000, //El tiempo que va a tener la cookie de vida
    },
    store: MongoStore.create({mongoUrl: DB_URL})//Para grabar la cookie.
}));

//Primero el session, y luego el passport//

//Para inicializar el passport y passport.session
server.use(passport.initialize());//Inicia Passport
server.use(passport.session());//Quiero que passport gestione mis sesiones

//Estas lineas lo que hacen es parsear la informacion que entra.
//Juan-> Esta línea, le dice a Express, que si recibe un (Post, PUT), con un JSON adjunto, lo lea
//y nos lo mande al endpoint dentro delreq.body
server.use(express.json());
//Si la peticion (Put, post...) y viene en formato urlendcoded, meta la informacion en el req.body;
server.use(express.urlencoded({
    extended: true
}));


//Rutas
server.use('/', indexRouter);
server.use('/data', moviesRouter);
server.use('/auth', authRouter);

//Controlador de errores (error handler)
server.use = ((error, req, res, next) => {
    //error.status = 500
    //error.message = "Unexpected error"

    const status = error.status || 500;
    const message = error.message || "Unexpected error! La que has liao no tiene nombre";

    return res.status(status).json(message);
})

server.listen(PORT, () => {
    console.log(`Servidor cargado y a punto en http://localhost:${PORT}`);
});