const path=require('path');//El path para los directorios públicos

const express = require('express');

const router = express.Router();

router.get('/', (req, res, next) => {
    try {
        return res.status(200).sendFile(path.resolve('./htmls/home.html'));
    }catch(error){
        return next (error);
    }
});

module.exports = router;
