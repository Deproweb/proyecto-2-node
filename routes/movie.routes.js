const express = require ("express");
const router = express.Router();

const MovieModel = require ("../models/Movies");
const CinemaModel = require ("../models/Cinema");

const {upload} = require('../middlewares/file.middleware');

    router.get('/movies',  async (req, res, next) => {

        try {
            const movies = await MovieModel.find();
            return res.status(200).json(movies);
            } catch (error) {
            return next (error);
            }
    });

    router.get('/id/:id', async (req, res, next) => {

        try {
            const id = req.params.id;
            const movies = await MovieModel.findById(id);
            return res.status(200).json(movies);
            } catch (error) {
                return next (error);
            }
    });

    router.get('/movies/:titulo', async (req, res, next) => {
        try {
            const titulo = req.params.titulo;
            const movies = await MovieModel.find({title: titulo});
            return res.status(200).json(movies);
            } catch (error) {
            return next (error);
            }
    });

    router.get('/genero/:genero', async (req, res, next) => {
        try {
            const genero = req.params.genero;
            const movies = await MovieModel.find({genre: genero});
            return res.status(200).json(movies);
            } catch (error) {
                return next (error);
            }
    });


    router.get('/age/:age', async (req, res, next) => {
        try {
            const age = req.params.age;
            const movies = await MovieModel.find({ year: {$gte:age} });
            return res.status(200).json(movies);
            } catch (error) {
                return next (error);
            }
    });


    router.delete('/delete/:id', async (req, res, next) => {
        try {
            const id = req.params.id;
            const movies = await MovieModel.findByIdAndDelete(id);
            if(movies){
                return res.status(200).json(`Deleted ${movies}`)
            }else{
                return res.status(200).json("You are trying delete an deleted movie")
            }
            } catch (error) {
                return next (error);
            }
    });

    //Se llama al middleware, en mitad de la ruta, con el upload.single(nombre-del-campo), que tiene que coindicidir con el parametro que le enviemos desde la req
    //ya que multer lo buscará en ese campo
    router.post('/create', [upload.single('image')] , async (req, res, next) => {

        //Para guardar nuevos datos en la base de datos,
        // 1.Creamos instancia del modelo
        // 2. Ejecutamos instancia.save() de la instancia.
        // 3. Hacemos lo que queramos con el dato
        // req = request
        // res = response
        // next = next action
        // req.body ---> POdemos recibir cualquier cosa req.body.titulo
        // Es distinto de el save del mongo
        const { title, director, year, genre, image } = req.body;

        const avatar = req.file? req.file.filename : null; //Como guardaremos el nombre de nuestro archivo

        try {
            console.log(req.body)
            console.log(req.file)

          // Crearemos una instancia de character con los datos enviados
        const newMovie = new MovieModel({
            title,
            director,
            year,
            genre,
            image : avatar
        });

        console.log(title, director, year, genre, image)
        //La otra forma:
        // name: req.body.name,
        // age: req.body.age,
        // alias: req.body.alias,
        // role: req.body.role

          // Guardamos el personaje en la DB
        const createdMovie = await newMovie.save();
        console.log("personaje añadido");
        return res.status(201).json(createdMovie);
        } catch (error) {
              // Lanzamos la función next con el error para que lo gestione Express
        return next(error);
        }
    });

    router.put('/edit/:id', async (req, res, next) => {
        try {
            console.log('He entrado en el put')
            const { id } = req.params //Recuperamos el id de la url
            console.log(id);
            const characterModify = new MovieModel(req.body) //instanciamos un nuevo Character con la información del body
            characterModify._id = id //añadimos la propiedad _id al personaje creado
            //el tercer argumento {new:true} nos devuelve el personaje ya actualizado.
            const characterUpdated = await MovieModel.findByIdAndUpdate(id , characterModify, {new: true})
            return res.status(200).json(characterUpdated)//Este personaje que devolvemos es el anterior a su modificación
        } catch (error) {
            return next(error)
        };
    });


    // router.get('/cinema', async (req, res, next) => {
    //     try{
    //         const cinemas = await CinemaModel.find();
    //         return res.status(200).json(cinemas);

    //     }catch(error){
    //         return next(error);
    //     }
    // });

    router.post('/create/cinema', async (req, res, next) => {
        try {

            const {name, location} = req.body;

            const newCinema = new CinemaModel({
                name,
                location,
                movies: []
            });

            const createdCinema = await newCinema.save();
            return res.status(201).json(createdCinema);
        } catch (error) {
            next(error);
        }
    });


    router.put('/add-movie', async (req, res, next) => {
        try {
            const {cinemaId, movieId} = req.body
            console.log("he entrado en el put del cinema")
            const updatedCinema = await CinemaModel.findByIdAndUpdate(
                cinemaId,
                // { $push: { movies: movieId } },
                { $addToSet: { movies: movieId }},//Método para no añadir el elemento en caso de que ya exista.
                { new: true }
            );
            return res.status(200).json(updatedCinema);
        } catch (error) {
            return next(error);
        }
    });


    router.delete('/delete-movie/:id', async (req, res, next) => {

        try{
            const {id} = req.body;

            const cinemaDeleted = await CinemaModel.findByIdAndDelete(id);
            console.log(cinemaDeleted);
            if(cinemaDeleted){
                return res.status(200).json(`Deleted ${cinemaDeleted}`)
            }else{
                return res.status(200).json("You are trying delete an deleted cinema")
            }

        }catch(error){
            return next(error);
        }
    })


    router.get('/cinema', async (req, res, next) => {
    try {
        console.log("He entrado en los cinemas");
        //Populate hace referencia al array creado donde has introducido datos
        const cinemas = await CinemaModel.find().populate('movies');
        //
        return res.status(200).json(cinemas)
    } catch (error) {
        return next(error)
    }
    });


    module.exports = router;