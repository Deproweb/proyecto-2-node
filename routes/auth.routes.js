const express = require('express');
const passport = require('passport');
const User = require('../models/User');
const router = express.Router();

router.post('/register', (req, res, next) => {

    try {
        const done = (error, savedUser) => {
            console.log(savedUser)
            if (error) {
                return next(error);
            }

            // req.logIn(user, (error) => {
            //     if (error) return next(error);

            //     return res.status(201).json(savedUser);
            // });
        };

        //Aqui conseguimos que se ejecute passport, con dos parámetros, el nombre de la estrategia, se conecta con el passport de la otra pagina, de register

        passport.authenticate('registro', done)(req);

    } catch (error) {
        console.log(error);
    };
});

router.post('/login', (req, res, next) => {
    try {
        const done = (error, user) => {
            if(error) {
                return next(error);
            }

            req.logIn(user, (error) => {
                if (error) return next(error);

                return res.status(200).json(user);
            });
        }

        passport.authenticate('login', done)(req);
    } catch (error) {
        return next(error);
    }
});

router.post('/logout', (req, res, next) => {
    if (req.user) {
        // deslogueo al usuario

        req.logout();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');

            return res.status(200).json('Usuario deslogueado');
        });
    } else {
        const error = new Error('No existe usuario logueado');
        return next(error);
    }
});


// El siguiente paso es crear un endpoint `/login` de tipo POST en el archivo `user.routes.js` para poder realizar el login.

// router.post('/login', (req, res, next) => {
//     passport.authenticate('login', (error, user) => {
//         if (error) return next(error)

//         req.logIn(user, (error) => { //El primer parametro es el usuario que nos pasa passport, desde login.strategy, y luego un error.
//             // Si hay un error logeando al usuario, resolvemos el controlador
//             if (error) {
//                 return next(error);
//             }
//             // Si no hay error, devolvemos al usuario logueado
//             return res.status(200).json(user)
//         });
//     })(req);
// });


module.exports = router;