const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CinemaSchema = new Schema ({

    name: {type: String, required: true},
    location: {type: String, required: true},
    //ref: Hace referencia al modelo con el que van a enlazar las Id que se almacenan en el array del casting.
    movies : [{ type: mongoose.Types.ObjectId, ref: 'Movie' }],//Aqui hace referencia al modelo, de las cosas que vas a meter aqui.
    },
    {
    timestamps: true,
    }
);

const CinemaModel = mongoose.model('Cinema', CinemaSchema);

module.exports = CinemaModel;


