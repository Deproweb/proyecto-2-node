//Se requiere mongoose, y se establece tanto la url como la config básica, ambos parámetros entran dentro del mongoose.connect

const mongoose = require("mongoose");

//Mongo siempre entra por el 27017, films es el nombre de la base de datos
const DB_URL = "mongodb://localhost:27017/films-proyect-2";
const CONFIG_DB = { useNewUrlParser: true, useUnifiedTopology: true};


//Se crea una función que conecte al llamarse con la base de datos:
//Se crean el try y el catch, dentro del try se coloca el await con la respuesta, y un desestructuring de lo que queremos de ella.
const connectToDb = async () => {

    try{
        const response = await mongoose.connect(DB_URL, CONFIG_DB);
        const {host, port, name} = response.connection;
        console.log(`Estamos conectados a ${name}, em ${host}:${port}`)
    }catch(error){
        console.log('Error al conectar en la DB', error);
    }
}

//Se exporta la función, para que sea ejecutada en el index y la url y la config
module.exports = {
    DB_URL,
    CONFIG_DB,
    connectToDb,
};