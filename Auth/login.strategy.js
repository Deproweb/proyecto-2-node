//Se requiere passport
const passport = require('passport');
//Se requiere la estrategia
const LocalStrategy = require('passport-local').Strategy;
//Se requiere el modelo de user.
const User = require('../models/User');
//Se requiere bcrypt
const bcrypt = require('bcrypt');


const validateEmail = (email) => {
    const re =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase()); //Respuesta booleana
};

const loginStrategy = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {
        try {

            const isValidEmail = validateEmail(email);
            if (!isValidEmail) {
                const error = new Error("Credenciales incorrectas");
                return done(error);
            }

            // Primero buscamos si el usuario existe en nuestra DB, el find one devuelve un único objeto, por eso aqui se niega su existencia.
            //no se niega su longitud, eso ocurre con el .find(), que lo que devuelve es un array.
            const actualUser = await User.findOne({
                email: email
            });

            // Si NO existe el usuario, enviamos un error, que creamos nosotros con el new Error, luego lo retornamos con done.
            if (!actualUser) {
                const error = new Error('Credentials are not correct');
                return done(error);
            }

            // Si existe el usuario, vamos a comprobar si su password enviado coincide con el registrado,
            // DE esto se encarga el modulo de bcrypt, el compare, que recibe el parametro en bruto, y el encriptado.
            //El bruto viene de la req, de la peticion. El encriptado lo traemos de la pagina de datos.
            const isValidPassword = await bcrypt.compare(
                password,
                actualUser.password
            );

            // Si el password no es correcto, enviamos un error a nuestro usuario, es una variable booleana!!!! Por eso tratamos
            //true o false.
            if (!isValidPassword) {
                const error = new Error(
                    'credentials are not correct!'
                );
                return done(error);
            }

            // Si todo se valida correctamente, eliminamos la contraseña del usuario devuelto
            // por la db y completamos el callback con el usuario
            actualUser.password = null;
            return done(null, actualUser);
        } catch (error) {
            // Si hay un error, resolvemos el callback con el error
            return done(error);
        }
    }
)
//Se exporta para que pueda cogerse en el index de auth
module.exports = loginStrategy;