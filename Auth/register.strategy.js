//Se requiere passport
const passport = require('passport');
//Se requiere la estrategia
const LocalStrategy = require('passport-local').Strategy;
//Se requiere el modelo de user.
const User = require('../models/User');
//Se requiere bcrypt
const bcrypt = require('bcrypt');

//Funciones de validacion de email, y password
const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(String(email).toLowerCase()); //Respuesta booleana
};

const validatePassword = (password) => {
    const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

    return re.test(String(password)); //Respuesta booleana
};


//Se crea una función en la que se instancia una nueva LocalStrategy, que lleva dos parámetros, un objeto y un callback
const registerStrategy = new LocalStrategy({
        //Cual es el campo que contiene el nombre de usuario, la contraseña y el callback.
        //Le decimos, que como campo del usernameFiel tome lo que viene en el modelo que se llama "email", y lo mismo con el password. El callback
        //se va a ejecutar cuando se llame a la estrategia de registro.
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    //Debajo van unos parámetros diferentes. El email y password vienen de arriba.
    async (req, email, password, done) => {
        /*
        1. Comprobar que el usuario no exista previamente.
        2. Encriptar la contraseña y guardarla en una variable.
        3. Creamos el usuario y lo guardamos en la bbdd.
        4. Le iniciamos la sesion al usuario llamando a passport.authenticate();
        */
        try {

            const isValidEmail = validateEmail(email);
            if (!isValidEmail) {
                const error = new Error('Email inválido, no me hagas trampas!');
                return done(error);
            }

            const isValidPassword = validatePassword(password);
            if (!isValidPassword) {
                const error = new Error('La contraseña tiene que contener de 6 a 16 carácteres, una mayúscula, minúscula y número');
                return done(error);
            }
            //Quiero que mire, en la bbdd, si existe dentro del User, un usario, que dentro de su email, tenga mi valor de email que viene desde arriba.
            const existingUser = await User.find({
                email: email
            }); //Esto devuelve un array, por eso se mira su longitud, si tiene elementos.

            //Se mira si el usario existe, a través de la longitud, pues el find devuelve un array.
            if (existingUser.length) {
                const error = new Error('The user is already registered');
                return done(error); //El error lo trata passport
            }

            //Se encripta la contraseña
            const saltRounds = 10;
            const hash = await bcrypt.hash(password, saltRounds);

            //Se crea el nuevo usuario
            const newUser = new User({
                email: email,
                password: hash,
                name: req.body.name
            });

            //Se guarda el usuario en la bbdd
            const savedUser = await newUser.save();

            //El primer argumento hace referencia al error, por eso en el primer parámetro se pasa null, por eso no hay error aquí.
            return done(null, savedUser);
        } catch (error) {
            done(error, null)
        };
    });

//Se exporta para que pueda cogerse en el index de auth
module.exports = registerStrategy;