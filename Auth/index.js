const passport = require('passport');
const User = require('../models/User')
const registerStrategy = require('./register.strategy');
const loginStrategy = require('./login.strategy');
const path=require('path');//El path para los directorios públicos
/*
1.Serializer
2.Deserializer
3.Passport.use(ambos)
*/
// Esta función usará el usuario de req.LogIn para registrar su id.
passport.serializeUser((user, done) => {
  return done(null, user._id);
});
// Esta función buscará un usuario dada su _id en la DB y populará req.user si existe
passport.deserializeUser(async (userId, done) => {
  try {
    const existingUser = await User.findById(userId);
    return done(null, existingUser);
  } catch (err) {
    return done(err);
  }
});


//Se ejecuta passport.use, con el nombre de la estrategia, que se ejecuta con el callback, que es la función.
passport.use('login', loginStrategy);
//Se ejecuta passport.use, con el nombre de la estrategia, que se ejecuta con el callback, que es la función.
passport.use('registro', registerStrategy);
