const path = require('path');
const fs = require('fs');
const multer = require('multer');


const storage = multer.diskStorage({
    filename : (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);//Para establecer el nombre del archivo
    },
    destination: (req, file, cb) => {
        //En el destino, queremos enviarlo a la carpeta public, y dentro a upload, así se hace a través de path.
        //Importante crear la carpeta de public y de upload
        //Con cloudinary,
        const directory = path.join(__dirname, '../public/upload' )//Dirname hace mencion a la raiz del proyecto hasta el lugar donde estamos ahora.
        cb(null, directory);
    }
});

const ACCEPTED_FILE_EXTENSIONS = ['image/jpeg', 'image/png', 'image/jpg'];

//Esta función es para filtrar el tipo de archivo que queremos permitir que se suba.
//Si el tipo de archivo no coincide, enviaremos un error.
const fileFilter = (req, file, cb) => {

    if (ACCEPTED_FILE_EXTENSIONS.includes(file.mimetype)){
        //Es un mimetype valido
        cb(null, true);
    }else{
        //No es un mimetype valido
        const error = new Error('Invalid mimetype');
        error.status = 400 // Bad request
        cb(error, null)
    }

};

//Esto es como si una misma constante, a través de multer, guarda los dos middleware
const upload = multer(
    {
        storage,
        fileFilter,
    }
);





module.exports = {upload}


//Si enviamos una imagen por un formulario, lo haremos a traves de:
// {/* <form action = "action" method="POST" enctype="multipart/form-data"></form> */}