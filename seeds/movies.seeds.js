//Se requiere mongoose, el modelo y la url/config

const mongoose = require('mongoose');
const MovieModel = require('../models/Movies');
const { DB_URL, CONFIG_DB } = require('../config/db');

//Se establece el array
const moviesArray = [
    {
        title: 'The Matrix',
        director: 'Hermanas Wachowski',
        year: 1999,
        genre: 'Acción',
    },
    {
        title: 'The Matrix Reloaded',
        director: 'Hermanas Wachowski',
        year: 2003,
        genre: 'Acción',
    },
    {
        title: 'Buscando a Nemo',
        director: 'Andrew Stanton',
        year: 2003,
        genre: 'Animación',
    },
    {
        title: 'Buscando a Dory',
        director: 'Andrew Stanton',
        year: 2016,
        genre: 'Animación',
    },
    {
        title: 'Interestelar',
        director: 'Christopher Nolan',
        year: 2014,
        genre: 'Ciencia ficción',
    },
    {
        title: '50 primeras citas',
        director: 'Peter Segal',
        year: 2004,
        genre: 'Comedia romántica',
    },
];

//Se conecta para guardar en la base de datos

/**
 * 1. Nos conectamos a la base de datos.
 * 2. Buscaremos si ya existen personajes.
 *      2.1 Si no existen, los creamos.
 *      2.2. Si existen personajes, borramos la colección y luego añadimos el charactersArray
 *
 * 3. Avisaremos al usuario si se han añadido personajes o hubo un error.
 *
 * 4. Cerraremos la conexión a la base de datos.
 */

mongoose.connect(DB_URL, CONFIG_DB)
    .then(async () => {
        console.log('Se está ejecutando el seed de las movies, todo va bien :D');

        //Aqui va a devolver todos los personajes de la coleccion
        const allMovies = await MovieModel.find();
        console.log(allMovies);

        //Si tiene elementos, borra todo
        if (allMovies.length){
            await MovieModel.collection.drop();
            console.log('Colección movies eliminada, para volver a llenar');
        }
    })
    .catch(error => console.log("Error buscando en la DB", error))
    .then(async () => {
        //Añadir el array a la base de datos
        await MovieModel.insertMany(moviesArray);
        console.log("Añadidas las nuevas peliculas")
    })
    .catch(error => console.log("Error añadiendo las pelis"))
    .finally(() => mongoose.disconnect());


